/**
 * Created by Ravinder Sahni on 6/5/14.
 */
var http = require('http'),
    url = require('url');

var items = [];

var server = http.createServer(function (req, res) {

    switch (req.method) {

        case 'POST':
            // The following command would be used to post an item:
            // curl -d "This is an item" http://localhost:3000
            var item = '';
            req.setEncoding('utf8');
            req.on('data', function (chunk) {
                item += chunk;
            });
            req.on('end', function () {
                items.push(item);
                res.end('OK\n');
            });
            break;

        case 'GET':
            // The following command would be used to retrieve the list of items:
            // curl http://localhost:3000
            var body = items.map(function(item, i) {
                return i + ') ' + item;
            }).join('\n');

            res.setHeader('Content-Length', Buffer.byteLength(body) + 1);
            res.setHeader('Content-Type', 'text/plain; charset="utf-8"');
            res.end(body + '\n');
            break;

        case 'PUT':
            // The following command would be used to update Item 2:
            // curl -X PUT -d "This is an item update" http://localhost:3000/1
            var path = url.parse(req.url).pathname;
            var i = Number(path.slice(1));
            var newItem = '';
            req.setEncoding('utf8');
            req.on('data', function(chunk) {
                newItem += chunk;
            });
            if (isNaN(i)) {
                res.statusCode = 400;
                res.end('Invalid item id\n');
            } else if (!items[i]) {
                res.statusCode = 404;
                res.end('Item not found\n');
            } else {
                req.on('end', function() {
                    items[i] = newItem;
                    res.end('OK\n');
                });
            }
            break;

        case 'DELETE':
            // The following command would be used to delete Item 1:
            // curl -X DELETE http://localhost:3000/0
            var path = url.parse(req.url).pathname;
            var i = Number(path.slice(1));

            if (isNaN(i)) {
                res.statusCode = 400;
                res.end('Invalid item id\n');
            } else if (!items[i]) {
                res.statusCode = 404;
                res.end('Item not found\n');
            } else {
                items.splice(i, 1);
                res.end('OK\n');
            }
            break;
    }
});

server.listen(3000);